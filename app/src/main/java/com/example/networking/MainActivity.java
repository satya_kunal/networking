package com.example.networking;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.networking.kunal.Kunal;

public class MainActivity extends AppCompatActivity {
private Button button_kunal,button_satya;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button_kunal=findViewById(R.id.button_kunal);
        button_satya=findViewById(R.id.button_satya);
        button_kunal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(), Kunal.class);
                startActivity(i);

            }
        });
        button_satya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i2=new Intent(getApplicationContext(),Satya.class);
                startActivity(i2);

            }
        });
    }
}
