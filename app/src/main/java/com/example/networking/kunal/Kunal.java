package com.example.networking.kunal;

import androidx.fragment.app.FragmentActivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.networking.R;

import java.io.IOException;

public class Kunal extends FragmentActivity implements Kunal_DownloadCallback {

    private static final String TAG ="Kunal" ;
    TextView show_Result;
    Button button_startDownload;
    ProgressBar progress_Bar;
    ImageButton connected_Image,noNetwork_Image;
    String holdData;
    Boolean aBoolean;


        // Keep a reference to the NetworkFragment, which owns the AsyncTask object
        // that is used to execute network ops.
        private Kunal_NetworkFragment networkFragment;

        // Boolean telling us whether a download is in progress, so we don't trigger overlapping
        // downloads with consecutive button clicks.
        private boolean downloading = false;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_kunal);
            button_startDownload=findViewById(R.id.startDownload);
            progress_Bar=findViewById(R.id.progressBar);
            show_Result=findViewById(R.id.showResult);
            noNetwork_Image=findViewById(R.id.noNetwork);
            connected_Image=findViewById(R.id.connectedImage);
            progress_Bar.setVisibility(View.INVISIBLE);
            connected_Image.setVisibility(View.INVISIBLE);
            noNetwork_Image.setVisibility(View.INVISIBLE);
            networkFragment = Kunal_NetworkFragment.getInstance(getSupportFragmentManager(), "https://api.myjson.com/bins/11zkvq");
            button_startDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    aBoolean=isOnline();

                    if(aBoolean) {
                        if (!downloading && networkFragment != null) {
                            Log.d(TAG, "!!! startdownloadclicked ");

                            progress_Bar.setVisibility(View.VISIBLE);

                            // Execute the async download.
                            networkFragment.startDownload();
                            downloading = true;

                        }
                    }
                    else {
                        noNetwork_Image.setVisibility(View.VISIBLE);
                        Log.d(TAG,"!!! no network ");
                    }
                }
            });


        }


    @Override
    public Void getData(String data) {
        Log.d(TAG,"!!! Data = "+data);


holdData=data;
        Log.d(TAG,"!!! Data is = "+data);
        return null;
    }


//    @Override
//    public Boolean progressBar(Boolean t) {
//
//        Log.d(TAG,"!!! progressbar ");
//
//        if(t) {
//            Log.d(TAG,"!!! progressbarVisible = ");
//
//
//            progress_Bar.setVisibility(View.VISIBLE);
//
//
//
//            }
//        else {
//            Log.d(TAG,"!!! progressbarInvisible = ");
//
//            progress_Bar.setVisibility(View.INVISIBLE);
//        }
//
//        return null;
//    }

//    @Override
//    public Boolean statusBar(Boolean t) {
//        Log.d(TAG,"!!! Statusbar ");
//try {
//    if (t) {
//
//
//        connected_Image.setVisibility(View.VISIBLE);
//        Log.d(TAG, "!!! StatusbarVisible = ");
//
//    } else {
//        Log.d(TAG, "!!! StatusbarInvisible = ");
//
//        connected_Image.setVisibility(View.INVISIBLE);
//    }
//
//}
//catch (Exception io)
//{
//    io.printStackTrace();
//
//}
//        return null;
//    }

    @Override
    public void updateFromDownload(Object result) {



        }

    @Override
    public NetworkInfo getActiveNetworkInfo(Boolean b) {



            return null;
    }


    @Override
    public void onProgressUpdate(int progressCode, int percentComplete) {
        switch(progressCode) {
            // You can add UI behavior for progress updates here.
            case Progress.ERROR:

                break;
            case Progress.CONNECT_SUCCESS:
                break;
            case Progress.GET_INPUT_STREAM_SUCCESS:

                break;
            case Progress.PROCESS_INPUT_STREAM_IN_PROGRESS:

                break;
            case Progress.PROCESS_INPUT_STREAM_SUCCESS:
                progress_Bar.setVisibility(View.INVISIBLE);
                connected_Image.setVisibility(View.VISIBLE);

                break;
        }

    }

    @Override
    public void finishDownloading() {
        Log.d(TAG,"!!!finishDownloading");
        show_Result.setText(holdData);
        connected_Image.setVisibility(View.VISIBLE);
        Log.d(TAG,"!!!aakhri");


    }

    public boolean isOnline() {
        try {
            Log.d(TAG,"!!! Is Online ?");

            ConnectivityManager connMgr;
            NetworkInfo networkInfo;

            connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            networkInfo = connMgr.getActiveNetworkInfo();
            aBoolean=(networkInfo != null && networkInfo.isConnected());
            Log.d(TAG,"!!! Is online ?");

            return (networkInfo != null && networkInfo.isConnected());


        } catch (NullPointerException e) {
            Log.d(TAG,"NullPointer Ex");
        }
        return aBoolean;
    }

}

