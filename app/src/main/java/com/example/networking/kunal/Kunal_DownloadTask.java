package com.example.networking.kunal;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Kunal_DownloadTask extends AsyncTask<String, Integer, Kunal_DownloadTask.Result> {

    private static final String TAG = "Kunal_DownloadTask";
    private Kunal_DownloadCallback<String> callback;
    private String data="",dataParsed="",singleParsed="";
    private Kunal_NetworkFragment kunal_networkFragment;
    private Boolean aBoolean;

    Kunal_DownloadTask(Kunal_DownloadCallback<String> callback) {
            setCallback(callback);
        }

        void setCallback(Kunal_DownloadCallback<String> callback) {
            this.callback = callback;
        }

        /**
         * Wrapper class that serves as a union of a result value and an exception. When the download
         * task has completed, either the result value or exception can be a non-null value.
         * This allows you to pass exceptions to the UI thread that were thrown during doInBackground().
         */
        static class Result {
            public String resultValue;
            public Exception exception;
            public Result(String resultValue) {
                this.resultValue = resultValue;
            }
            public Result(Exception exception) {
                this.exception = exception;
            }

        }

        /**
         * Cancel background network operation if we do not have network connectivity.
         */


        @Override
        protected void onPreExecute() {
            Log.d(TAG,"!!! onPreExecute ");
//            callback.progressBar(true);




//            try {
//                aBoolean=kunal_networkFragment.isOnline();
//                Log.d(TAG,"!!! aboolean took status of internet");
//
//                if (aBoolean) {
//                    callback.getActiveNetworkInfo(true);
//                } else {
//                    callback.getActiveNetworkInfo(false);
//                    kunal_networkFragment.cancelDownload();
//                    this.cancel(true);
//                }
//            }
//            catch(Exception e)
//            {
//                Log.d(TAG,"!!! Exception");
//
//            }
        }

        /**
         * Defines work to perform on the background thread.
         */
        @Override
        protected Kunal_DownloadTask.Result doInBackground(String... urls) {
            Result result = null;
            Log.d(TAG,"!!! doinBackground ");

            if (!isCancelled() && urls != null && urls.length > 0) {
                String urlString = urls[0];
                try {
                    URL url = new URL(urlString);
                    String resultString = downloadUrl(url);
                    if (resultString != null) {
                        result = new Result(resultString);
                        Log.d(TAG,"!!! Data = "+result.resultValue);

//                        callback.progressBar(false);
                        callback.onProgressUpdate(1,20);
                        callback.getData(result.resultValue);

                    }
                } catch(Exception e) {
                    callback.onProgressUpdate(-1,0);
                    result = new Result(e);
                }
            }

            return result;
        }

        /**
         * Updates the DownloadCallback with the result.
         */
        @Override
        protected void onPostExecute(Result result) {
            if (result != null && callback != null) {
                if (result.exception != null) {
                    callback.updateFromDownload(result.exception.getMessage());
                } else if (result.resultValue != null) {
                    callback.updateFromDownload(result.resultValue);
                }
                callback.onProgressUpdate(3,100);
                callback.finishDownloading();

//                callback.progressBar(false);

            }
        }

        /**
         * Override to add special behavior for cancelled AsyncTask.
         */
        @Override
        protected void onCancelled(Result result) {
        }

    private String downloadUrl(URL url) throws IOException, JSONException {
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line = "";

                while (line != null) {
                    line = bufferedReader.readLine();
                    data = data + line;
                }
                JSONArray jsonArray = new JSONArray(data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                    singleParsed = "Name = " + jsonObject.get("name") + "\n" +
                            " Age = " + jsonObject.get("age") + "\n" +
                            "Address = " + jsonObject.get("address") + "\n" +
                            "Relationship = " + jsonObject.get("relationship") + "\n";
                    dataParsed = dataParsed + singleParsed;


                }
                callback.onProgressUpdate(2,75);
//                callback.statusBar(true);

            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }




        return dataParsed;
    }


    public String readStream(InputStream stream, int maxReadSize)
            throws IOException, UnsupportedEncodingException {
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");
        char[] rawBuffer = new char[maxReadSize];
        int readSize;
        StringBuffer buffer = new StringBuffer();
        while (((readSize = reader.read(rawBuffer)) != -1) && maxReadSize > 0) {
            if (readSize > maxReadSize) {
                readSize = maxReadSize;
            }
            buffer.append(rawBuffer, 0, readSize);
            maxReadSize -= readSize;
        }
        return buffer.toString();
    }
}
