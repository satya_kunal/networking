package com.example.networking.kunal;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import com.example.networking.R;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

public class Kunal_NetworkFragment extends Fragment {

        public static final String TAG = "NetworkFragment";

        private static final String URL_KEY = "UrlKey";

        private Kunal_DownloadCallback<String> callback;
        private Kunal_DownloadTask downloadTask;
        private String urlString;
        private boolean aBoolean=false;

        /**
         * Static initializer for NetworkFragment that sets the URL of the host it will be downloading
         * from.
         */
        public static Kunal_NetworkFragment getInstance(FragmentManager fragmentManager, String url) {
            Kunal_NetworkFragment networkFragment = new Kunal_NetworkFragment();
            Bundle args = new Bundle();
            args.putString(URL_KEY, url);
            networkFragment.setArguments(args);
            fragmentManager.beginTransaction().add(networkFragment, TAG).commit();
            return networkFragment;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            urlString = getArguments().getString(URL_KEY);

        }

        @Override
        public void onAttach( Context context) {
            super.onAttach(context);
            // Host Activity will handle callbacks from task.
            callback = (Kunal_DownloadCallback<String>) context;
        }

        @Override
        public void onDetach() {
            super.onDetach();
            // Clear reference to host Activity to avoid memory leak.
            callback = null;
        }

        @Override
        public void onDestroy() {
            // Cancel task when Fragment is destroyed.
            cancelDownload();
            super.onDestroy();
        }

        /**
         * Start non-blocking execution of DownloadTask.
         */
        public void startDownload() {
            cancelDownload();
            Log.d(TAG,"!!! startdownload() inside fragment n urlstring= "+urlString);

            downloadTask = new Kunal_DownloadTask(callback);
            downloadTask.execute(urlString);
        }

        /**
         * Cancel (and interrupt if necessary) any ongoing DownloadTask execution.
         */
        public void cancelDownload() {
            if (downloadTask != null) {
                downloadTask.cancel(true);
            }
        }
    public boolean isOnline() {
        try {
            Log.d(TAG,"!!! Is Online ");

            ConnectivityManager connMgr;
            NetworkInfo networkInfo;

            connMgr = (ConnectivityManager)
                    getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            networkInfo = connMgr.getActiveNetworkInfo();
            aBoolean=(networkInfo != null && networkInfo.isConnected());
            Log.d(TAG,"!!! Is online");

            return (networkInfo != null && networkInfo.isConnected());


        } catch (NullPointerException e) {
            Log.d(TAG,"NullPointer Ex");
        }
    return aBoolean;
}

    }

